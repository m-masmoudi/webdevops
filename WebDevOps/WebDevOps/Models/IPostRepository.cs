﻿using WebDevOps.Models;

namespace WebDevOps.Repository
{
    public interface IPostRepository
    {
        List<PostViewModel> GetPosts();
    }
}
